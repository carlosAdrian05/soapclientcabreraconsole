﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SOAPClientCabreraConsole.Interfaces
{
    public interface IPedimentoService
    {
        Task<List<string>> GetReferenciasAll();
        Task GetReferenciaInformation();
        void SendInformation(PedimentoW.PedimentosW[] ListPedimento, string sReferencia);
        
    }
}
