﻿using AutoMapper;
using SOAPClientCabreraConsole.Entities;
using SOAPClientCabreraConsole.PedimentoW;

namespace SOAPClientCabreraConsole.Mapping
{
    public class AutoMapping : Profile
    {

        public static MapperConfiguration Mapping()
        {
            //Se crea el mappeo
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<FN_ANEXO24_WB_SKF_Patidas_Result, PedimentoW.PedimentosW>();
            });

            return config;
        }
        //public AutoMapping()
        //{
        //    CreateMap<PedimentoW.PedimentosW, FN_ANEXO24_WB_SKF_Patidas_Result>();
        //    CreateMap<FN_ANEXO24_WB_SKF_Patidas_Result, PedimentoW.PedimentosW>();
        //}
    }
}
