﻿using SOAPClientCabreraConsole.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOAPClientCabreraConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            PedimentoService service = new PedimentoService();

            Console.WriteLine("Inicio envio de archivos " + DateTime.UtcNow );
            Task.FromResult
                (service.GetReferenciaInformation());
            Console.WriteLine("Fin envio de archivos " + DateTime.UtcNow );


            return;
        }
    }
}
