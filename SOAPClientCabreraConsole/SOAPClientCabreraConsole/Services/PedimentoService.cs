﻿using AutoMapper;
using SOAPClientCabreraConsole.Entities;
using SOAPClientCabreraConsole.Interfaces;
using SOAPClientCabreraConsole.PedimentoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOAPClientCabreraConsole.Services
{
    public class PedimentoService : IPedimentoService
    {
        private static SysExpertEntities SysExpertEntities;
        private static CLLAIntranetNewEntities CLLAIntranetNewEntities;
        private IMapper _mapper;

        public PedimentoService(
            SysExpertEntities sysExpertEntities,
            CLLAIntranetNewEntities cLLAIntranetNewEntities)
        {
            SysExpertEntities = sysExpertEntities;
            CLLAIntranetNewEntities = cLLAIntranetNewEntities;
        }
        public PedimentoService() 
        {
            if(SysExpertEntities is null)
                SysExpertEntities = new SysExpertEntities();

            if (CLLAIntranetNewEntities is null)
                CLLAIntranetNewEntities = new CLLAIntranetNewEntities();
        }

        public async Task<List<string>> GetReferenciasAll()
        {
            var referencias = await Task.FromResult(SysExpertEntities.FN_ANEXO24_WB_SKF_Patidas(
                               null,
                               null)
                            .Select(s => s.NumeroReferencia)
                            .Distinct()
                            .ToList());

            var referenciasSent = await Task.FromResult( CLLAIntranetNewEntities.LogEnvioSKFWS
                                    .Where(w => w.Estatus == 202)
                                    .Select(s => s.Referencia)
                                    .Distinct()
                                    .ToList());

            var result = referencias.Except(referenciasSent).ToList();
            
            return result;
        }

        public async Task GetReferenciaInformation()
        {
            _mapper = Mapping.AutoMapping
                        .Mapping()
                        .CreateMapper();

            var listReferencias = await GetReferenciasAll();

            foreach (var item in listReferencias) 
            {
                var referenciaSend = SysExpertEntities.FN_ANEXO24_WB_SKF_Patidas(
                                        item,
                                        null)
                                    .ToArray();

                var referenciaSendDTO = _mapper.Map<PedimentosW[]>(referenciaSend);

                var referencia = referenciaSend.Select(s => s.NumeroReferencia).FirstOrDefault();

                SendInformation(referenciaSendDTO, referencia);

            }
        }

        public void SendInformation(PedimentoW.PedimentosW[] ListPedimento, string sReferencia)
        {
            PedimentoW.Credenciales creden = new PedimentoW.Credenciales();
            creden.SUsuario = "C.Llamas";
            creden.SPassword = "BQpvTKNF6WDY";


            PedimentoW.PedimentoWSSoapClient request = new PedimentoW.PedimentoWSSoapClient();

            var Respuesta = request.IntegracionPedimento(ListPedimento, creden);

            GenerateLog(Respuesta, ListPedimento, sReferencia);

        }

        private bool GenerateLog(PedimentoW.Respuesta[] respuesta, PedimentoW.PedimentosW[] ListPedimento, string Sreferencia)
        {
            using (var ctx = new CLLAIntranetNewEntities())
            {
                for (int j = 0; j < ListPedimento.Count(); j++)
                {

                    var log = new LogEnvioSKFWS()
                    {
                        Pedimento = null,
                        Referencia = Sreferencia,
                        Descripcion = respuesta[0].SMenaje,
                        Estatus = (int)respuesta[0].IEstatus,
                        FechaEnvio = DateTime.Now,
                        Factura = ListPedimento[j].SFactura,
                        Partida = ListPedimento[j].SNumeroParte
                    };

                    ctx.LogEnvioSKFWS.Add(log);
                    ctx.SaveChanges();
                }

                return true;
            }
        }
    }
}
